let imgArr = [
    "images/dog1.jpg",
    "images/dog2.jpg",
    "images/cat.jpg",
    "images/duck.jpg"
],
    imgText = ["Sleepy", "Puppy", "Puffy", "Newborn"],

    prevBtn = document.getElementById("prev"),
    nextBtn = document.getElementById("next"),
    pauseBtn = document.getElementById("pause"),
    eleImg = document.getElementById("img"),
    ctrlBtn = document.getElementById("img_control");

const playText = "<i class=\"fas fa-play\"></i>",
      pauseText = "<i class=\"fas fa-pause\"></i>",
      interval = 1000;

let counter = 0,
    slider = null;

function driveImg() {
    if (counter >= imgArr.length) {
        counter = 0;
    }
    eleImg.src = imgArr[counter++];
    imgSelected(counter - 1);
}

function pauseSlider() {
    if (slider) {
        clearInterval(slider);
        slider = null;
        counter--;
        pauseBtn.innerHTML = playText;
    } else {
        slider = setInterval(driveImg, interval);
        pauseBtn.innerHTML = pauseText;
    }
}

function addImgTag() {
    for (let i = 0; i < imgArr.length; i++) {
        let btn = document.createElement("button");
        btn.addEventListener("click", clickBtn => {
            let id = clickBtn.target.id;
            counter = parseInt(id, 10);
            eleImg.src = imgArr[counter];
            imgSelected(counter);
            clearInterval(slider);
            isPlay();
        })
        btn.id = i.toString();
        ctrlBtn.appendChild(btn);
        btn.classList.add('ctrlBtn');
        btn.innerHTML = imgText[i];
    }
}

function isPlay() {
    if (slider) {
        slider = null;
        pauseBtn.innerHTML = playText;
    }
}

function imgSelected(idx) {
    imgArr.forEach((val, i) => {
        let btnId = i.toString(),
            btn = document.getElementById(btnId);
        btn.style.backgroundColor = idx === i? "rgba(0,0,0,0.1)" : "";
    })
}

prevBtn.addEventListener("click", clickBtn => {
    if (slider) {
        clearInterval(slider);
        counter--;
    }
    if (counter === 0) {
        counter = imgArr.length;
    }
    eleImg.src = imgArr[--counter];
    imgSelected(counter);
    isPlay();
})

nextBtn.addEventListener("click", clickBtn => {
    if (slider) {
        clearInterval(slider);
        counter--;
    }
    if (counter >= imgArr.length - 1) {
        counter = -1;
    }
    eleImg.src = imgArr[++counter];
    imgSelected(counter);
    isPlay();
})

addImgTag();
slider = setInterval(driveImg, interval);